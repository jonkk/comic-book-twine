Stories = new Meteor.Collection("stories");
Panels = new Meteor.Collection("panels");
PanelElements = new Meteor.Collection("panelelements");
Statistics = new Meteor.Collection("statistics");

Router.map( function () {
  this.route('stats', {
    data: function() {
      return Statistics.findOne();
    }
  });
  this.route('story', {
    path: '/:_id',
    data: function() { 
      Session.set("story_id", this.params._id);
      return Stories.findOne(this.params._id);
    }
  });
  this.route('home', {
    path: '/',
    action: function() {
      // if root path URL is entered, create a new story and change url to reflect the id of the new story

      /*
      var panelindex = Stories.find(story_id, {fields: { panelcount: 1 }}).fetch();
      var panel_id = Panels.insert({ story: story_id, index: panelindex});
      Stories.update(story_id, { $inc: { panelcount: 1 }});
      PanelElements.insert({ panel: panel_id, type: "description", text: "This is a description"});
      PanelElements.insert({ panel: panel_id, type: "choice", text: "This is a choice"});
      PanelElements.insert({ panel: panel_id, type: "choice", text: "This is a choice"});*/
      var story_id = Stories.insert({ panelcount: 0, text: "A STORY!" });
      Meteor.call('stats', 1, "start");
      //Statistics.update({name:"stats"},{$inc: {start: 1}}, {multi:true});
      Session.set("story_id", story_id);
      Meteor.call('addstory', story_id, "start");
      Router.go('story', {_id: story_id});
    }
  });
});

if (Meteor.isClient) {
  preloadimages = function(arr) {
    var newimages=[];
    var arr=(typeof arr!="object")? [arr] : arr; //force arr parameter to always be an array
    for (var i=0; i<arr.length; i++){
        newimages[i]=new Image()
        newimages[i].src=arr[i]
    }
  }
  //preload 3 images:
  preloadimages(['panel_01.png', 'panel_02.png', 'panel_03.png', 
                  'panel_04.png', 'panel_05.png', 'panel_06.png', 
                  'panel_07.png', 'panel_08.png', 'panel_09.png', 
                  'panel_10.png', 'panel_11.png', 'panel_12.png', 
                  'panel_13.png', 'panel_14.png', 'panel_15.png', 
                  'panel_16.png', 'panel_17.png', 'panel_18.png', 
                  'panel_19.png', 'panel_20_bible.png', 'panel_20_gun.png',
                  'panel_21a.png', 'panel_21b.png', 'panel_22a.png',
                  'panel_22b.png', 'panel_23a.png', 'panel_23b.png',
                  'panel_24a.png', 'panel_24b.png', 'panel_25a.png',
                  'panel_26b.png', 'small_box.png', 'small_box_choice.png',
                  'small_bubble_down_choice.png', 'small_bubble_down.png', 'small_bubble_right_choice.png', 'small_bubble_right.png', 'the_end.png',
                  'x_small_bubble_left_choice.png', 'x_small_bubble_left.png', 'xsmall_bubble.png', 'rotinhell.png', 'rotinhell_choice.png',
                  'bible_choice.png', 'bible.png', 'graveyard.png', 'gun_choice.png', 'gun.png', 'header.png', 'large_box.png', 'medium_box_choice.png',
                  'medium_box.png', 'medium_bubble_down_choice.png', 'medium_bubble_down.png', 'medium_bubble_left_choice.png', 'medium_bubble_left.png',
                  'medium_bubble_right_cover.png', 'medium_bubble_right.png']);

  Template.story.panel = function () {
    return Panels.find({story: Session.get("story_id")}, {sort: {index: 1}});
    //return 0;
    //return Panels.find();
  };

  Template.story.panelContent = function () {
    //console.log("Panel: " + this._id);
    return PanelElements.find({panel: this._id});
  };

  Template.story.isType = function (type) {
    //console.log("Panel Element: " + this._id);
    var panel = PanelElements.findOne(this._id);
    if (panel.type == type) return true;
    else return false;
  }

  Template.story.events({
    'click .choice': function () {
      var link = PanelElements.findOne(this._id).link; 
      Meteor.call('addstory', Session.get("story_id"), link, function (error, result) {
        $.scrollTo( '#panel'+result, 800);
        //$("#panel"+result).scrollIntoView();
        //console.log('#panel'+result);
      });
      //var lastpanel = Stories.findOne(Session.get("story_id")).panelcount.toString();
    }
  });

  Deps.autorun(function () {
    Meteor.subscribe("panels", Session.get("story_id"));
    Meteor.subscribe("panelelements", Session.get("story_id"));
  });
  Meteor.subscribe("all-stories");
  Meteor.subscribe("all-stats");
}

if (Meteor.isServer) {
  Meteor.publish("panels", function (storyId) {
    check(storyId, String);
    return Panels.find({story: storyId});
  });

  Meteor.publish("panelelements", function(storyId) {
    check(storyId, String);
    return PanelElements.find({story: storyId});
  });

  Meteor.publish("all-stories", function () {
    return Stories.find();
  });

  Meteor.publish("all-stats", function () {
    return Statistics.find();
  });

  Meteor.methods({

    stats: function (amount, type) {
      switch(type) {
        case "start":
        Statistics.update({name:"stats"},{$inc: {start: amount}});
        break;

        case "finish":
        Statistics.update({name:"stats"},{$inc: {finish: amount}});
        break;
      }
    },

    addstory: function (story_id, choice) {
      clearChoices(story_id);
      var elements;
      var first;
      var last;
      switch (choice) {
        case "start":
          elements = [
          {type: "image", name: "panel_01"}
          ];
          first = addPanel(elements, story_id);

          elements = [
          {type: "image", name: "panel_02"}
          ];
          addPanel(elements, story_id);

          elements = [
          {type: "image", name: "panel_03"}
          ];
          addPanel(elements, story_id);

          elements = [
          {type: "image", name: "panel_04"},
          {type: "choice", text: "yes?", image: "x_small_bubble_left", width:"60", x: "64", y:"64", link: "inn"}
          ];
          last = addPanel(elements, story_id);
        break;

        case "inn":
          elements = [
          {type: "image", name: "panel_05"},
          {type: "speech", text: "I have a letter for a Mr.. Am-", image: "medium_bubble_left", width:"88", padding:"5px 35px", x: "25", y:"-5" }
          ];
          addPanel(elements, story_id);

          elements = [
          {type: "image", name: "panel_06"},
          {type: "choice", text: "Mr. Amsel", image: "small_bubble_down", width:"91", x: "-12", y:"5", link: "ravenintro_a"},
          {type: "choice", text: "Around here we call him..", image: "medium_bubble_down", width:"88", padding:"5px 35px", x: "64", y: "28", link: "ravenintro_b"}
          ];
          last = addPanel(elements, story_id);
        break;

        case "ravenintro_a":
          elements = [
          {type: "image", name: "panel_07"},
          {type: "speech", text: '"..The Raven.', image: "small_box", width:"120", x:"15", y:"15"},
          {type: "speech", text: '"..I hear he eats children.', image: "medium_box", width:"240", x:"145", y:"20"}
          ];
          addPanel(elements, story_id);

          elements = [
          {type: "image", name: "panel_08"},
          {type: "speech", text: '"..Nah, he and his brothers were cursed by their father..', image: "large_box", width: "180", padding:"0 15px", x:"-3", y:"5"},
          {type: "speech", text: '"..for failing to aid their sister on her deathbed."', image: "large_box", width: "180", padding:"0 15px", x:"-3", y:"200"}
          ];
          addPanel(elements, story_id);

          elements = [
          {type: "image", name: "panel_09"}
          ];
          addPanel(elements, story_id);

          elements = [
          {type: "image", name: "panel_10"},
          {type: "choice", text: "Could it really be true?", image: "medium_bubble_left", width:"88", padding:"5px 35px", x: "-25", y:"5", link: "thechoice"}
          ];
          last = addPanel(elements, story_id);
        break;

        case "ravenintro_b":
          elements = [
          {type: "image", name: "panel_07"},
          {type: "speech", text: '"..The Goat.', image: "small_box", width:"120", x:"15", y:"15"},
          {type: "speech", text: '"..Nah, it\'s The Raven.', image: "medium_box", width:"240", x:"145", y:"20"}
          ];
          addPanel(elements, story_id);

          elements = [
          {type: "image", name: "panel_08"},
          {type: "speech", text: '".He and his brothers were cursed by their father..', image: "large_box", width: "180", padding:"0 15px", x:"-3", y:"5"},
          {type: "speech", text: '"..for failing to aid their sister on her deathbed."', image: "large_box", width: "180", padding:"0 15px", x:"-3", y:"200"}
          ];
          addPanel(elements, story_id);

          elements = [
          {type: "image", name: "panel_09"}
          ];
          addPanel(elements, story_id);

          elements = [
          {type: "image", name: "panel_10"},
          {type: "choice", text: "Could it really be true?", image: "medium_bubble_left", width:"88", padding:"5px 35px", x: "-25", y:"5", link: "thechoice"}
          ];
          last = addPanel(elements, story_id);
        break;

        case "thechoice":
          elements = [
          {type: "image", name: "panel_11"},
          {type: "speech", text: "My sister!", image: "small_bubble_down", width:"91", x: "25", y:"10"},
          ];
          addPanel(elements, story_id);

          elements = [
          {type: "image", name: "panel_12"},
          {type: "choice", image: "bible", x: "15", y:"58", link: "bible"},
          {type: "choice", image: "gun", x: "125", y:"130", link: "gun"}
          ];
          last = addPanel(elements, story_id);
        break;

        case "bible":
          Stories.update(story_id, {$set: {gun: false}});
          elements = [
          {type: "image", name: "panel_13"},
          {type: "choice", text: '"She said to meet her at our old home..', image: "medium_box", width:"240", x: "5", y:"5", link: "home"},
          {type: "choice", text: '"But first I should pay my brothers a visit..', image: "medium_box", width:"240", x: "210", y:"220", link: "graveyard"}
          ];
          last = addPanel(elements, story_id);
        break;

        case "gun":
          Stories.update(story_id, {$set: {gun: true}});
          elements = [
          {type: "image", name: "panel_13"},
          {type: "choice", text: '"She said to meet her at our old home..', image: "medium_box", width:"240", x: "-35", y:"5", link: "home"},
          {type: "choice", text: '"But first I should pay my brothers a visit..', image: "medium_box", width:"240", x: "210", y:"220", link: "graveyard"}
          ];
          last = addPanel(elements, story_id);
        break;

        case "graveyard":
          elements = [
          {type: "image", name: "graveyard"},
          {type: "choice", text: '"..where they lie."', image: "small_box", width:"120", x: "15", y:"15", link:"home2"}
          ];
          last = addPanel(elements, story_id, "two");
        break;

        case "home":
          elements = [
          {type: "image", name: "panel_14"},
          {type: "speech", text: '"..where father lives."', image: "small_box", width:"120", x: "15", y:"15"}
          ];
          addPanel(elements, story_id);

          elements = [
          {type: "image", name: "panel_15"},
          {type: "speech", text: 'Brother!', image: "small_bubble_down", width:"91", x: "105", y:"-5"}
          ];
          addPanel(elements, story_id);

          elements = [
          {type: "image", name: "panel_16"},
          {type: "speech", text: "Father is ill. He rests, but is near death.", image: "medium_bubble_down", width:"120", padding:"5px 20px", x: "25", y:"15"}
          ];
          addPanel(elements, story_id);

          elements = [
          {type: "image", name: "panel_17"},
          {type: "speech", text: "Father. I beg your forgiveness..", image: "medium_bubble_left", width:"120", padding:"5px 20px", x: "125", y:"-20"},
          {type: "speech", text: "...", image: "small_bubble_down", width:"91", x: "270", y:"90"}
          ];
          addPanel(elements, story_id, "three");

          elements = [
          {type: "image", name: "panel_18"},
          {type: "choice", text: "I..", image: "small_bubble_down", width:"91", x: "65", y:"15", link:"hell"}
          ];
          last = addPanel(elements, story_id);
        break;

        case "home2":
          elements = [
          {type: "image", name: "panel_14"}
          //{type: "speech", text: '"..where father lives."', image: "small_box", width:"120", x: "15", y:"15", link:"home2"}
          ];
          addPanel(elements, story_id);

          elements = [
          {type: "image", name: "panel_15"},
          {type: "speech", text: 'Brother!', image: "small_bubble_down", width:"91", x: "105", y:"-5"}
          ];
          addPanel(elements, story_id);

          elements = [
          {type: "image", name: "panel_16"},
          {type: "speech", text: "Father is ill. He rests, but is near death.", image: "medium_bubble_down", width:"120", padding:"5px 20px", x: "25", y:"15"}
          ];
          addPanel(elements, story_id);

          elements = [
          {type: "image", name: "panel_17"},
          {type: "speech", text: "Father. I beg your forgiveness..", image: "medium_bubble_left", width:"120", padding:"5px 20px", x: "125", y:"-20"},
          {type: "speech", text: "...", image: "small_bubble_down", width:"91", x: "270", y:"90"}
          ];
          addPanel(elements, story_id, "three");

          elements = [
          {type: "image", name: "panel_18"},
          {type: "choice", text: "I..", image: "small_bubble_down", width:"91", x: "65", y:"15", link:"hell"}
          ];
          last = addPanel(elements, story_id);
        break;

        case "hell":
          elements = [
          {type: "image", name: "panel_19"},
          {type: "choice", image: "rotinhell", x: "310", y:"80", link: "action"}
          ];
          last = addPanel(elements, story_id);
        break;

        case "action":
          Statistics.update({name:"stats"},{$inc: {finish: 1}});

          if (Stories.findOne(story_id).gun) {

            //if player has a gun

            elements = [
            {type: "image", name: "panel_20_gun"}
            ];
            addPanel(elements, story_id);

            elements = [
            {type: "image", name: "panel_21a"},
            {type: "speech", text:"I borrowed your gun.", image: "medium_bubble_right", width:"120", padding:"15px 20px", x: "-10", y:"-15"}
            ];
            addPanel(elements, story_id);

            elements = [
            {type: "image", name: "panel_22a"},
            {type: "speech", text: "ungh!", image: "x_small_bubble_left", width:"60", x: "124", y:"20"}
            ];
            addPanel(elements, story_id);

            elements = [
            {type: "image", name: "panel_23a"}
            ];
            addPanel(elements, story_id, "three");

            elements = [
            {type: "image", name: "panel_24a"},
            {type: "choice", text: "I'm free!", image: "small_bubble_down", width:"91", x: "5", y:"-4", link:"goodend" }
            ];
            last = addPanel(elements, story_id);

          }

          else

          {
            //if the player has the bible

            elements = [
            {type: "image", name: "panel_20_bible"}
            ];
            addPanel(elements, story_id);

            elements = [
            {type: "image", name: "panel_21b"},
            {type: "choice", text: "RUN!", image:"small_bubble_right", width:"91", x: "50", y:"-4", link:"badend_1"}
            ];
            last = addPanel(elements, story_id);

            
          }
        break;

        case "badend_1":
            elements = [
            {type: "image", name: "panel_22b"}
            ];
            addPanel(elements, story_id);

            elements = [
            {type: "image", name: "panel_23b"}
            ];
            addPanel(elements, story_id);

            elements = [
            {type: "image", name: "panel_24b"},
            {type: "choice", text:"I'm so sorry my brothers..", image: "medium_bubble_down", width:"120", padding:"15px 20px", x: "5", y:"-20", link:"badend_2"}
            ];
            last = addPanel(elements, story_id, "three");

            
        break;

        case "badend_2":
            elements = [
            {type: "image", name: "panel_25b"}
            ];
            addPanel(elements, story_id);

            elements = [
            {type: "image", name: "panel_26b"},
            {type: "speech", image: "the_end", x: 170, y: 250}
            ];
            last = addPanel(elements, story_id);
        break;

        case "goodend":

            elements = [
            {type: "image", name: "panel_25a"},
            {type: "speech", image: "the_end", x: 380, y: 250}
            ];
            last = addPanel(elements, story_id, "two");
        break;
      }
      return last.toString();
    } 
  });

  Meteor.startup(function () {
    if (Statistics.find().count() === 0) {
      Statistics.insert({name: "stats", start:5, finish:0});
    }
    // code to run on server at startup
  });

  addPanel = function(elements, story_id, number) {
    
    var panelindex = Stories.findOne(story_id).panelcount + 1;
    var panel_id = Panels.insert({story: story_id, index: panelindex, num: number, visible: 'hidden'});
    _.each( elements, function(elm) {
      var element = PanelElements.insert(elm);
      PanelElements.update(element, {$set: {panel: panel_id, story: story_id}});
    });
    Panels.update(panel_id, {$set: {visible: 'visible'}});
    Stories.update(story_id, { $inc: { panelcount: 1 }});

    return panelindex;
  };

  clearChoices = function(story_id) {
    PanelElements.update({story: story_id, type:"choice"}, {$set: {type: "speech"}}, {multi:true});
  };
}

